import kuebalok as kb

from kivy.app import App
#kivy.require("1.8.0")
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button

from random import seed
from random import randint

class MainScreen(GridLayout):
    def __init__(self, **kwargs):
        super(MainScreen, self).__init__(**kwargs)
        self.cols = 3

        # row 1
        self.add_widget(Label(text="Input:"))

        self.txtIn = TextInput(multiline=True)
        self.add_widget(self.txtIn)

        self.encryptButton = Button(text='Encrypt!', on_press=self.OnEncryptButtonPressed)
        self.add_widget(self.encryptButton)

        # row 2
        self.add_widget(Label(text="Output:"))

        self.txtOut = TextInput(multiline=True)
        self.add_widget(self.txtOut)

        self.decryptButton = Button(text='Decrypt!', on_press=self.OnDecryptButtonPressed)
        self.add_widget(self.decryptButton)

        # row 3
        self.add_widget(Label(text="PassKey:"))
        self.passKey = TextInput(multiline=False, password=True)
        self.add_widget(self.passKey)
    
    def OnEncryptButtonPressed(self, instance):
        pk = self.passKey.text
        print('Encrypted!')
        self.txtOut.text = kb.encrypt(pk,self.txtIn.text)
    
    def OnDecryptButtonPressed(self, instance):
        pk = self.passKey.text
        self.txtIn.text = kb.decrypt(pk,self.txtOut.text)
        print('Decrypted!')

class KueBalok(App):
    def build(self):
        return MainScreen()





if __name__ == "__main__":
    KueBalok().run()
