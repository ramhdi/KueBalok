from random import seed
from random import randint

def addchar(chr_in, inc):
    chr_res = chr_in + inc
    if (chr_res < 0):
        chr_res += 26
    elif (chr_res > 25):
        chr_res -= 26
    return chr_res

def encrypt(str_seed, str_in):
    seed(str_seed)
    output = ""
    size = len(str_in)
    ch = 0
    inc = 0

    while (ch < size):
        res = ord(str_in[ch])
        inc = randint(0,25)
        if (ord(str_in[ch]) >= 65 and ord(str_in[ch]) <= 90):
            res = addchar(ord(str_in[ch]) - 65, inc) + 65
        elif (ord(str_in[ch]) >= 97 and ord(str_in[ch]) <= 122):
            res = addchar(ord(str_in[ch]) - 97, inc) + 97
        
        output += chr(res)
        ch += 1
    
    return output

def decrypt(str_seed, str_in):
    seed(str_seed)
    output = ""
    size = len(str_in)
    ch = 0
    inc = 0

    while (ch < size):
        res = ord(str_in[ch])
        inc = randint(0,25) * -1
        if (ord(str_in[ch]) >= 65 and ord(str_in[ch]) <= 90):
            res = addchar(ord(str_in[ch]) - 65, inc) + 65
        elif (ord(str_in[ch]) >= 97 and ord(str_in[ch]) <= 122):
            res = addchar(ord(str_in[ch]) - 97, inc) + 97
        
        output += chr(res)
        ch += 1
    
    return output